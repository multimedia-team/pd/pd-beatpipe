lib.name = beatpipe
class.sources = beatpipe.c 
datafiles = \
	$(wildcard *.pd) \
	$(wildcard *.txt) \
	$(empty)

PDLIBBUILDER_DIR=/usr/share/pd-lib-builder
include $(PDLIBBUILDER_DIR)/Makefile.pdlibbuilder
